"""
File model .pth
File feat  .npy
File label .npy

Mỗi subfolder được lưu tại ./model/{epoch:int}
Các model .pth là state dict của các backbone 
Các feat.npy là 1 matrix (numrow x 512) mỗi row là feat của 1 face có kích thước 512, chưa normalize
Các label.npy là 1 vector kích thước = numrow, chứa index của các class, 0, 1, 2, 3, ... maxID


Tạo ra 1 dataframe với các cột, 
index1 | index 2 | L2 dist | cosin dist 
"""

from typing import Tuple, Any
from util.utils import perform_val, evaluate, gen_plot, buffer_val
from util.verification import calculate_roc
from PIL import Image
from sklearn.utils import shuffle
import numpy as np
from numpy.random import shuffle as shuffle_
from numpy.core import sqrt as sqrt_
def normalize(x:np.ndarray) -> np.float32:
    x = x.ravel(order='K').astype('float32')
    return x / sqrt_(x.dot(x))


def evaluate(embeddings, actual_issame, nrof_folds = 10, pca = 0):
    # Calculate evaluation metrics
    thresholds = np.arange(0, 4, 0.01)
    embeddings1 = embeddings[0::2]
    embeddings2 = embeddings[1::2]
    tpr, fpr, accuracy, best_thresholds = calculate_roc(thresholds, embeddings1, embeddings2, np.asarray(actual_issame), nrof_folds = nrof_folds, pca = pca)
    # thresholds = np.arange(0, 4, 0.001)
    # val, val_std, far = calculate_val(thresholds, embeddings1, embeddings2,
    #                                   np.asarray(actual_issame), 1e-3, nrof_folds=nrof_folds)
    # return tpr, fpr, accuracy, best_thresholds, val, val_std, far
    return tpr, fpr, accuracy, best_thresholds

def validate(embeddings:np.ndarray, issame:np.ndarray, 
             metric:str='l2', nrof_folds:int = 10):
    assert metric in ('l2', 'cosin')
    assert embeddings.shape[0] == 2*issame.shape[0]
    
    tpr, fpr, accuracy, best_thresholds = evaluate(embeddings, issame, nrof_folds)
    buf = gen_plot(fpr, tpr)
    roc_curve = Image.open(buf)
    return accuracy.mean(), best_thresholds.mean(), roc_curve
    # roc_curve_tensor = to_tensor(roc_curve)
    # return accuracy.mean(), best_thresholds.mean(), roc_curve_tensor


# def load_pair(feat_path:str, label_path:str) -> Tuple[np.ndarray, np.ndarray]:
    
#     embeddings = np.load(feat_path)     # 26.8 Mb
#     for row in range(len(embeddings)):
#         embeddings[row] = normalize(embeddings[row])
        
#     label = np.load(label_path)
#     issame = []
#     for i in range(0, len(label), 2):
#         x = label[i] == label[i+1]
#         issame.append(x)
        
#     issame = np.array(issame)
#     # print(embeddings.shape)
#     # print(issame.shape)
#     return embeddings, issame


def load_pair(feat_path:str, label_path:str, bs:int = 512, _shuffle:bool=False, random_state=None
              ) -> Tuple[np.ndarray, np.ndarray]:
    embeddings = np.load(feat_path)     # 26.8 Mb
    for row in range(len(embeddings)):
        embeddings[row] = normalize(embeddings[row])
        
    labels = np.load(label_path)
    if _shuffle:
        embeddings, labels = shuffle(embeddings, labels, random_state=random_state)
    feats = []
    issame = []
    
    start = 0
    stop = bs
    num_face = embeddings.shape[0]
    while start < num_face:
        for i in range(start, stop):
            for j in range(i+1, stop):  
                feats.append(embeddings[i])
                feats.append(embeddings[j])
                _same = labels[i] == labels[j]
                issame.append(_same.__bool__())
                
        start += bs
        stop += bs
        stop = min(num_face, stop)
        
    # print('len(issame) = ', len(issame))
    return np.array(feats), np.array(issame)
    
def get_same_n_diff_index(arr:np.ndarray, value:Any, num_same:int, num_diff:int
                          ) -> Tuple[np.ndarray, np.ndarray, int]:
    """
    lấy random ra 1 số lượng num_same/num_diff các index giống/khác giá trị với giá trị cho trước
    """
    same = arr == value
    diff = np.logical_not(same)
    
    same = np.asarray(np.where(same) [0]); shuffle_(same)
    diff = np.asarray(np.where(diff) [0]); shuffle_(diff)
    
    same = same[:num_same]
    
    same_size = len(same)
    if same_size < num_same:
        num_diff = 32 - same_size
    diff = diff[:num_diff]
    return same, diff, same_size + len(diff)


def create_pair(feats:np.ndarray, labels:np.ndarray, bs:int = 32
                ) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    
    feat1 , feat2 , is_same = [], [], []
    start = 0
    stop = bs
    num_face = feats.shape[0]
    while start < num_face:
        for i in range(start, stop):
            for j in range(i+1, stop):  
                feat1.append(feats[i])
                feat2.append(feats[j])
                is_same.append(labels[i] == labels[j])
                
        start += bs
        stop += bs
        stop = min(num_face, stop)
    print(len(is_same))
    return np.array(feat1), np.array(feat2), np.array(is_same)


def load_n_create_pair(feat_path:str, label_path:str) -> Tuple[np.ndarray, np.ndarray]:
    embeddings = np.load(feat_path)     # 26.8 Mb
    for row in range(len(embeddings)):
        embeddings[row] = normalize(embeddings[row])
    label = np.load(label_path)
    
    embeddings1 , embeddings2 , actual_issame = create_pair(embeddings, label, 128)
    thresholds = np.arange(0, 4, 0.01)

    tpr, fpr, accuracy, best_thresholds = \
        calculate_roc(thresholds, embeddings1, embeddings2, np.asarray(actual_issame), nrof_folds = 10, pca = 0)
    return tpr, fpr, accuracy, best_thresholds


def run(random_state):
    embeddings, issame = load_pair('model/56/feat.npy', 'model/56/label.npy',
                                   bs=128, _shuffle=True, random_state=random_state)
    acc, thres, roc_curve = validate(embeddings, issame)

    print(acc * 100, thres)
    # roc_curve.save('model/56/roc_curve.png')

def run2():
    tpr, fpr, accuracy, best_thresholds = load_n_create_pair('model/1/feat.npy', 'model/1/label.npy')
    acc = accuracy.mean()
    thres = best_thresholds.mean()
    
    buf = gen_plot(fpr, tpr)
    roc_curve = Image.open(buf)
    print(acc * 100, thres)
    roc_curve.save('model/1.roc_curve.png')
    # return accuracy.mean(), best_thresholds.mean(), roc_curve

def test():
    np.random.seed(0)
    x = np.random.rand(3, 6) * 3
    print(x)
    print()
    for row in range(len(x)):
        x[row] = normalize(x[row])
    print(x)
    return


for i in range(50):
    run(1000*i)