import os, posixpath as P
from time import time
from glob import iglob
from itertools import chain, cycle
from random import choice, random
from PIL import Image, ImageFile
import numpy as np
import cv2
import dlib
from MaskTheFace.utils.aux_functions import download_dlib_model, mask_opencv_image

MASKTHEFACE_ROOT = P.dirname(__file__)
# r = lambda: randint(0, 255)

class changeWorkingDirectory(object):
    def __init__(self, path:str=MASKTHEFACE_ROOT):
        self.wd = path
        self.prev = os.getcwd()
        
    def __enter__(self):
        os.chdir(self.wd)
        
    def __exit__(self, t, v, tb):
        os.chdir(self.prev)


class Mask_transform(object):
    def __init__(
        self, 
        mask_type:str= "surgical",  # choices=["surgical", "N95", "KN95", "cloth", "gas", "inpaint", "random", "all"]
        pattern:str='',             # Type of the pattern. Available options in masks/textures
        # pattern_weight:float=0.5,   # Weight of the pattern. Must be between 0 and 1
        # color_weight:float=0.5,     # Weight of the color intensity. Must be between 0 and 1
        # code:str = 'cloth-masks/textures/floral/floral_1.png', # https://github.com/aqeelanwar/MaskTheFace#1---code
        p:float =0.5
        ) -> None:
        super().__init__()
        
        self.detector = dlib.get_frontal_face_detector()
        path_to_dlib_model = P.join(MASKTHEFACE_ROOT, 
                                    'dlib_models/shape_predictor_68_face_landmarks.dat')
        if not P.exists(path_to_dlib_model):
            with changeWorkingDirectory(MASKTHEFACE_ROOT):
                download_dlib_model()
        self.predictor = dlib.shape_predictor(path_to_dlib_model)
        self.textures = cycle(chain(
                            iglob(P.join( MASKTHEFACE_ROOT, 'masks/textures', '**/*.png')),
                            iglob(P.join( MASKTHEFACE_ROOT, 'masks/textures', '**/*.jpg')) ))
        self.p = p
        self.color = ''
        self.color_weight = ''
        self.reset()

        # self.pattern = pattern
        # self.pattern_weight = pattern_weight
        
        # self.color_weight = color_weight
        # self.code = code
        
        # mask_code = "".join(code.split()).split(",")
        # self.code_count = np.zeros(len(mask_code))
        # self.mask_dict_of_dict = {}
        
        
        # for i, entry in enumerate(mask_code):
        #     mask_dict = {}
        #     mask_color = ""
        #     mask_texture = ""
        #     mask_type = entry.split("-")[0]
        #     if len(entry.split("-")) == 2:
        #         mask_variation = entry.split("-")[1]
        #         if "#" in mask_variation:
        #             mask_color = mask_variation
        #         else:
        #             mask_texture = mask_variation
        #     mask_dict["type"] = mask_type
        #     mask_dict["color"] = mask_color
        #     mask_dict["texture"] = mask_texture
        #     self.mask_dict_of_dict[i] = mask_dict


    def reset(self):
        self.mask_type = choice(["surgical", "N95", "KN95", "cloth"])
        self.pattern_weight = random()
        self.code = '{mask_type}-{texture}'.format(
                      mask_type=self.mask_type, texture=next(self.textures))
        
        mask_code = "".join(self.code.split()).split(",")
        self.code_count = np.zeros(len(mask_code))
        self.mask_dict_of_dict = {}
        
        for i, entry in enumerate(mask_code):
            mask_dict = {}
            mask_color = ""
            mask_texture = ""
            mask_type = entry.split("-")[0]
            if len(entry.split("-")) == 2:
                mask_variation = entry.split("-")[1]
                if "#" in mask_variation:
                    mask_color = mask_variation
                else:
                    mask_texture = mask_variation
            mask_dict["type"] = mask_type
            mask_dict["color"] = mask_color
            mask_dict["texture"] = mask_texture
            self.mask_dict_of_dict[i] = mask_dict
        
        
    def __call__(self, pil_img:ImageFile.ImageFile):
        if random() < self.p:
            self.reset()
            opencv_img = cv2.cvtColor(np.array(pil_img), 4) #cv2.COLOR_RGB2BGR
            with changeWorkingDirectory(MASKTHEFACE_ROOT):
                masked = mask_opencv_image(opencv_img, self)
                
            if len(masked) < 1: return pil_img
            
            return Image.fromarray(cv2.cvtColor(masked[0], 4)) #cv2.COLOR_BGR2RGB
        
        return pil_img

if __name__ == '__main__':

    model = Mask_transform(p=2.)
    img = Image.open('/home/tz/Documents/MaskTheFace/datasets/feats/1/Duc_0_face_0.jpg')
    s = time()
    img = model(img)
    ss = time()

    print('runtime = ', ss - s)
    img.show('feafe')

