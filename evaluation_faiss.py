import os, shutil, posixpath as P
from re import X
from glob import glob, iglob
from typing import Tuple
import numpy as np
import faiss

from numpy.core import sqrt as sqrt_
def normalize(x:np.ndarray) -> np.float32:
    x = x.ravel(order='K').astype('float32')
    return x / sqrt_(x.dot(x))


def load_data(feat_path:str, label_path:str) -> Tuple[np.ndarray, np.ndarray]:
    feats = np.load(feat_path)     # 26.8 Mb
    labels = np.load(label_path)
    for row in range(len(feats)):
        feats[row] = normalize(feats[row])
    
    return feats, labels



class Matching:
    d = 512
    index = faiss.IndexFlatL2(d)
    
    def __init__(self, data_path:str, label_path:str):
        datas, labels = load_data(data_path, label_path)
        datas = datas.astype('float32')
        self.index.add(datas) 
        self.datas = datas
        self.labels = labels
        
    def search(self, querry:np.ndarray, k=4) -> Tuple[np.ndarray, np.ndarray]:
        dist, idx = self.index.search(querry.reshape(1, -1), k)
        return dist, idx
    
    def search_batch(self, querrys:np.ndarray, k=4) -> Tuple[np.ndarray, np.ndarray]:
        dists, idxs = self.index.search(querrys, k)
        return dists, idxs

matching = Matching('model/56/feat.npy', 'model/56/label.npy')
labels = matching.labels
datas = matching.datas

def search_and_compare(index) -> bool:
    qry = datas[index]
    gt = labels[index]
    dist, idx = matching.search(qry, 4)
    pred = np.take(labels, idx)[0][1:] # bỏ kq search đầu tiên do == qry
    cmp = pred == gt
    return cmp.sum() >= 2

def run_search_and_compare() -> float:
    c = 0
    size = datas.shape[0]
    for idx in range(size):
        c += search_and_compare(idx)
    
    acc = 1.*c / size 
    print(acc)  # 0.9983240223463687
    return acc


if __name__ == '__main__':
    run_search_and_compare()