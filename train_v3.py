import os
import posixpath as P
from typing import Tuple

import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
import torch.utils.data.sampler as sample
import torchvision.datasets as datasets
import torchvision.transforms as T
from chefboost import Chefboost as chef
from rich import print
from scipy.spatial.distance import cosine as cosine_distance
from sklearn.metrics.pairwise import cosine_similarity
# from tensorboardX import SummaryWriter
from tqdm import tqdm

from backbone.model_irse import IR_50
from backbone.model_resnet import ResNet_50
from config import configurations
from head.metrics import ArcFace
from loss.focal import FocalLoss
from util.utils import (AverageMeter, accuracy, get_time,
                        make_weights_for_balanced_classes,
                        schedule_lr, separate_irse_bn_paras,
                        separate_resnet_bn_paras, warm_up_lr)
from MaskTheFace.mask_transform import Mask_transform, changeWorkingDirectory
import warnings
warnings.filterwarnings("once")

#!======= hyperparameters & data loaders =======#
cfg = configurations[2]

SEED                 = cfg['SEED'                ] # random seed   for reproduce results
DATA_ROOT            = cfg['DATA_ROOT'           ] # the    parent     root      where   your train/val/test data are stored
MODEL_ROOT           = cfg['MODEL_ROOT'          ] # the    root       to        buffer  your checkpoints
LOG_ROOT             = cfg['LOG_ROOT'            ] # the    root       to        log     your train/val status
BACKBONE_RESUME_ROOT = cfg['BACKBONE_RESUME_ROOT'] # the    root       to        resume  training from a saved checkpoint
HEAD_RESUME_ROOT     = cfg['HEAD_RESUME_ROOT'    ] # the    root       to        resume  training from a saved checkpoint

BACKBONE_NAME  = cfg['BACKBONE_NAME'  ] # support: ['ResNet_50', 'ResNet_101', 'ResNet_152', 'IR_50', 'IR_101', 'IR_152', 'IR_SE_50', 'IR_SE_101', 'IR_SE_152']
HEAD_NAME      = cfg['HEAD_NAME'      ] # support: ['Softmax' , 'ArcFace' , 'CosFace', 'SphereFace', 'Am_softmax' ]
LOSS_NAME      = cfg['LOSS_NAME'      ] # support: ['Focal' , 'Softmax' ]

INPUT_SIZE     = cfg['INPUT_SIZE'     ]
RGB_MEAN       = cfg['RGB_MEAN'       ] # for normalize inputs
RGB_STD        = cfg['RGB_STD'        ]
EMBEDDING_SIZE = cfg['EMBEDDING_SIZE' ] # feature dimension

BATCH_SIZE     = cfg['BATCH_SIZE'     ]
DROP_LAST      = cfg['DROP_LAST'      ] # whether drop the last batch to ensure consistent batch_norm statistics
LR             = cfg['LR'             ] # initial LR
NUM_EPOCH      = cfg['NUM_EPOCH'      ]
EPOCH_START    = cfg['EPOCH_START'    ]
WEIGHT_DECAY   = cfg['WEIGHT_DECAY'   ]
MOMENTUM       = cfg['MOMENTUM'       ]
STAGES         = cfg['STAGES'         ] # epoch stages to decay learning rate
DEVICE         = cfg['DEVICE'         ]
MULTI_GPU      = cfg['MULTI_GPU'      ] # flag to use multiple GPUs
GPU_ID         = cfg['GPU_ID'         ] # specify your GPU ids
PIN_MEMORY     = cfg['PIN_MEMORY'     ]
NUM_WORKERS    = cfg['NUM_WORKERS'    ]


def prepare_data() -> Tuple[data.DataLoader, data.DataLoader, int]:
    print('create dataset, dataloader , caculate num_class')
    train_transform = T.Compose([
        T.Resize([int(128 * INPUT_SIZE[0] / 112), int(128 * INPUT_SIZE[0] / 112)]), # smaller side resized
        Mask_transform(p=0.65),
        T.RandomCrop([INPUT_SIZE[0], INPUT_SIZE[1]]),
        T.RandomHorizontalFlip(),
        T.ToTensor(),
        T.Normalize(mean = RGB_MEAN, std = RGB_STD),
    ])

    val_transform = T.Compose([
        T.Resize(INPUT_SIZE),
        T.ToTensor(),
        T.Normalize(mean = RGB_MEAN, std = RGB_STD),
    ])
    
    train_dataset = datasets.ImageFolder(P.join(DATA_ROOT, 'train'), train_transform)
    val_dataset = datasets.ImageFolder(P.join(DATA_ROOT, 'val'), val_transform)
    NUM_CLASS = len(train_dataset.classes)
    
    # create a weighted random sampler to process imbalanced data
    weights = make_weights_for_balanced_classes(train_dataset.imgs, len(train_dataset.classes))
    weights = torch.DoubleTensor(weights)
    sampler = sample.WeightedRandomSampler(weights, len(weights))
    g = torch.Generator()
    g.manual_seed(SEED)
    
    train_loader = data.DataLoader(
        train_dataset, batch_size = BATCH_SIZE, sampler = sampler, pin_memory = PIN_MEMORY,
        num_workers = NUM_WORKERS, drop_last = DROP_LAST, shuffle = False, generator=g
    )
    val_loader = data.DataLoader(
        val_dataset, batch_size = BATCH_SIZE, sampler = None, pin_memory = PIN_MEMORY,
        num_workers = NUM_WORKERS, drop_last = DROP_LAST, shuffle = True, generator=g
    )
    return train_loader, val_loader, NUM_CLASS


def create_network(NUM_CLASS) -> Tuple[nn.Module, nn.Module, nn.Module, optim.Optimizer]:
    print('creating network')
    #!======= model & loss & optimizer =======#
    BACKBONE_DICT = {'ResNet_50': ResNet_50(INPUT_SIZE), 
                    #  'ResNet_101': ResNet_101(INPUT_SIZE), 
                    #  'ResNet_152': ResNet_152(INPUT_SIZE),
                     'IR_50': IR_50(INPUT_SIZE), 
                    #  'IR_101': IR_101(INPUT_SIZE), 
                    #  'IR_152': IR_152(INPUT_SIZE),
                    #  'IR_SE_50': IR_SE_50(INPUT_SIZE), 
                    #  'IR_SE_101': IR_SE_101(INPUT_SIZE), 
                    #  'IR_SE_152': IR_SE_152(INPUT_SIZE)
    }
    BACKBONE = BACKBONE_DICT[BACKBONE_NAME]
    
    HEAD_DICT = {'ArcFace': ArcFace(in_features = EMBEDDING_SIZE, out_features = NUM_CLASS, device_id = GPU_ID),
                #  'CosFace': CosFace(in_features = EMBEDDING_SIZE, out_features = NUM_CLASS, device_id = GPU_ID),
                #  'SphereFace': SphereFace(in_features = EMBEDDING_SIZE, out_features = NUM_CLASS, device_id = GPU_ID),
                #  'Am_softmax': Am_softmax(in_features = EMBEDDING_SIZE, out_features = NUM_CLASS, device_id = GPU_ID)
                 }
    HEAD = HEAD_DICT[HEAD_NAME]


    LOSS_DICT = {'Focal':   FocalLoss(), 
                 'Softmax': nn.CrossEntropyLoss(),
                #  'AdaCos' : AdaCos(feat_dim= ..., num_classes= NUM_CLASS)
                #  'AdaM_Softmax': AdaM_Softmax() ,
                #  'ArcFace' :        ArcFace() ,
                #  'ArcNegFace':      ArcNegFace(),
                #  'CircleLoss':      CircleLoss(),
                #  'CurricularFace':  CurricularFace(),
                #  'MagFace' :        MagFace(),
                #  'NPCFace' :        MV_Softmax.py(),
                #  'SST_Prototype':   SST_Prototype(),
                }
    LOSS = LOSS_DICT[LOSS_NAME]


    if BACKBONE_NAME.find("IR") >= 0:
        backbone_paras_only_bn, backbone_paras_wo_bn = separate_irse_bn_paras(BACKBONE) # separate batch_norm parameters from others; do not do weight decay for batch_norm parameters to improve the generalizability
        _, head_paras_wo_bn = separate_irse_bn_paras(HEAD)
    else:
        backbone_paras_only_bn, backbone_paras_wo_bn = separate_resnet_bn_paras(BACKBONE) # separate batch_norm parameters from others; do not do weight decay for batch_norm parameters to improve the generalizability
        _, head_paras_wo_bn = separate_resnet_bn_paras(HEAD)
    
    #TODO optim.AdamW 
    # https://github.com/deepinsight/insightface/blob/master/recognition/arcface_torch/train.py/#L95
    OPTIMIZER = optim.SGD([{'params': backbone_paras_wo_bn + head_paras_wo_bn, 'weight_decay': WEIGHT_DECAY}, 
                           {'params': backbone_paras_only_bn}], 
                           lr = LR, momentum = MOMENTUM)
    # OPTIMIZER = optim.AdamW([{'params': backbone_paras_wo_bn + head_paras_wo_bn, 'weight_decay': WEIGHT_DECAY}, 
    #                          {'params': backbone_paras_only_bn}],
    #                         lr = LR, weight_decay=0.1)  #https://github.com/deepinsight/insightface/blob/master/recognition/arcface_torch/configs/base.py#L34
    # print(OPTIMIZER)
    # print("Optimizer Generated")

    #! optionally resume from a checkpoint
    if BACKBONE_RESUME_ROOT and P.isfile(BACKBONE_RESUME_ROOT):
        print("Loading Backbone Checkpoint '{}'".format(P.relpath(BACKBONE_RESUME_ROOT, os.getcwd() )))
        BACKBONE.load_state_dict(torch.load(BACKBONE_RESUME_ROOT))
    else:
        print("BACKBONE Checkpoint not found at '{}'".format(BACKBONE_RESUME_ROOT))
    
    if HEAD_RESUME_ROOT and P.isfile(HEAD_RESUME_ROOT):
        print("Loading Head Checkpoint '{}'".format(P.relpath(HEAD_RESUME_ROOT, os.getcwd() )))
        try:
            HEAD.load_state_dict(torch.load(HEAD_RESUME_ROOT))
        except:
            print('HEAD Checkpoint from different dataset')
    else:
        print("HEAD Checkpoint not found at '{}'".format(HEAD_RESUME_ROOT))

    
    if MULTI_GPU:
        BACKBONE = nn.DataParallel(BACKBONE, device_ids = GPU_ID)
        BACKBONE = BACKBONE.to(DEVICE)
    else:
        BACKBONE = BACKBONE.to(DEVICE)
        
    return BACKBONE, HEAD, LOSS, OPTIMIZER


torch.manual_seed(SEED)
if not P.isdir(MODEL_ROOT): 
    os.makedirs(MODEL_ROOT)
# writer = SummaryWriter(LOG_ROOT) # writer for buffering intermedium results
train_loader, val_loader, NUM_CLASS = prepare_data()
BACKBONE, HEAD, LOSS, OPTIMIZER = create_network(NUM_CLASS)


@torch.no_grad()
@torch.inference_mode()
def valid(epoch:int):
    BACKBONE.eval(); HEAD.eval(); LOSS.eval()
    losses = AverageMeter()
    df = pd.DataFrame(columns = ['decision', 'distance'])
    
    for inputs, labels in tqdm(iter(val_loader), desc='Validating '):
        inputs = inputs.to(DEVICE)
        labels_cuda = labels.to(DEVICE).long()
        features = BACKBONE(inputs)
        feats = features.detach().cpu().numpy()
        outputs = HEAD(features, labels_cuda)
        loss = LOSS(outputs, labels_cuda)
        losses.update(loss.data.item(), inputs.size(0))
    
        start = 0
        stop = 32
        num_face = feats.shape[0]
        while start < num_face:
            for i in range(start, stop):
                for j in range(i+1, stop):  
                    dist = cosine_distance(feats[i].reshape(512), feats[j].reshape(512))
                    decision = 'Yes' if labels[i] == labels[j] else 'No'
                    df = df.append({'decision':decision, 'distance':dist}, 
                                    ignore_index=True)
            
            start += 32
            stop += 32
            stop = min(num_face, stop)

    df.to_csv(P.join(MODEL_ROOT, '{}.csv'.format(epoch)), index=None)
    tp_mean = round(df[df.decision == "Yes"].mean().values[0], 4)
    tp_std = round(df[df.decision == "Yes"].std().values[0], 4)
    fp_mean = round(df[df.decision == "No"].mean().values[0], 4)
    fp_std = round(df[df.decision == "No"].std().values[0], 4)
    
    #! save and print
    print("Mean of true positives: ", tp_mean)
    print("Std of true positives: ", tp_std)
    print("Mean of false positives: ", fp_mean)
    print("Std of false positives: ", fp_std)
    
    print('Valid Loss {loss.val:.4f} {loss.avg:.4f}\n'.format(loss=losses))
    for sigma in (2, 3):
        print('sigma = {} -> threshold = {}'.format(sigma, round(tp_mean + sigma * tp_std, 4)))
    
    tmp_df = df[['distance', 'decision']].rename(columns = {"decision": "Decision"}).copy()
    model = chef.fit(tmp_df, {'algorithm': 'C4.5', 'enableParallelism':False})
    
    
@torch.enable_grad()
def train(epoch:int, NUM_EPOCH_WARM_UP, NUM_BATCH_WARM_UP, DISP_FREQ, number_of_batches, batch) -> None:
    # adjust LR for each training stage after warm up, 
    # you can also choose to adjust LR manually (with slight modification) once plaueau observed
    if epoch in STAGES: 
        schedule_lr(OPTIMIZER)
    BACKBONE.train()  # set to training mode
    HEAD.train()
    LOSS.train()

    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()
    print(' ',end='\n')
    for inputs, labels in tqdm(iter(train_loader), desc='Training'):
        if (epoch + 1 <= NUM_EPOCH_WARM_UP) and (batch + 1 <= NUM_BATCH_WARM_UP): # adjust LR for each training batch during warm up
            warm_up_lr(batch + 1, NUM_BATCH_WARM_UP, LR, OPTIMIZER)

        # compute output
        inputs = inputs.to(DEVICE)
        labels = labels.to(DEVICE).long()
        features = BACKBONE(inputs)
        outputs = HEAD(features, labels)
        loss = LOSS(outputs, labels)

        # measure accuracy and record loss
        prec1, prec5 = accuracy(outputs.data, labels, topk = (1, 5))
        losses.update(loss.data.item(), inputs.size(0))
        top1.update(prec1.data.item(), inputs.size(0))
        top5.update(prec5.data.item(), inputs.size(0))

        # compute gradient and do SGD step
        OPTIMIZER.zero_grad()
        loss.backward()

        OPTIMIZER.step()
        
        # dispaly training loss & acc every DISP_FREQ
        if ((batch + 1) % (DISP_FREQ if DISP_FREQ != 0 else 28) == 0) and batch != 0:
            print('Epoch {}/{} Batch {}/{}\n'
                  'Training Loss {loss.val:.4f} ({loss.avg:.4f})\n'
                  'Training Prec@1 {top1.val:.3f} ({top1.avg:.3f})\n'
                  'Training Prec@5 {top5.val:.3f} ({top5.avg:.3f})'.format(
                epoch + 1, NUM_EPOCH, batch + 1, number_of_batches * NUM_EPOCH, loss = losses, top1 = top1, top5 = top5))

        batch += 1 # batch index

    """training statistics per epoch (buffer for visualization)
    epoch_loss = losses.avg
    epoch_acc = top1.avg
    writer.add_scalar("Training_Loss", epoch_loss, epoch + 1)
    writer.add_scalar("Training_Accuracy", epoch_acc, epoch + 1)
    """
    
    print('Epoch: {}/{}\n'
            'Training Loss {loss.val:.4f} ({loss.avg:.4f})\n'
            'Training Prec@1 {top1.val:.4f} ({top1.avg:.4f})\n'
            'Training Prec@5 {top5.val:.4f} ({top5.avg:.4f})\n'.format(
        epoch + 1, NUM_EPOCH, loss = losses, top1 = top1, top5 = top5))


def run():
    #!======= train & validation & save checkpoint =======#
    number_of_batches = len(train_loader)
    DISP_FREQ = number_of_batches // 100 # frequency to display training loss & acc

    NUM_EPOCH_WARM_UP = NUM_EPOCH // 25  # use the first 1/25 epochs to warm up
    NUM_BATCH_WARM_UP = number_of_batches * NUM_EPOCH_WARM_UP  # use the first 1/25 epochs to warm up
    batch = 0  # batch index
    
    for epoch in range(EPOCH_START, NUM_EPOCH):
        train(epoch, NUM_EPOCH_WARM_UP, NUM_BATCH_WARM_UP, DISP_FREQ, number_of_batches, batch)
        valid(epoch)
        
        if MULTI_GPU:
            torch.save(BACKBONE.module.state_dict(), P.join(MODEL_ROOT, "Backbone_{}_Epoch_{}_Batch_{}_{}.pth".format(BACKBONE_NAME, epoch + 1, batch, get_time())))
        else:
            torch.save(BACKBONE.state_dict(), P.join(MODEL_ROOT, "Backbone_{}_Epoch_{}_Batch_{}_{}.pth".format(BACKBONE_NAME, epoch + 1, batch, get_time())))
        torch.save(HEAD.state_dict(), P.join(MODEL_ROOT, "Head_{}_Epoch_{}_Batch_{}_{}.pth".format(HEAD_NAME, epoch + 1, batch, get_time())))

if __name__ == '__main__':
    run()
    