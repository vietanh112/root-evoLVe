import os
import posixpath as P
from typing import Tuple

import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
import torch.utils.data.sampler as sample
import torchvision.datasets as datasets
import torchvision.transforms as T
from chefboost import Chefboost as chef
from rich import print
from copy import deepcopy
from tqdm import tqdm
from scipy.spatial.distance import cosine as cosine_distance
from sklearn.metrics.pairwise import cosine_similarity
# from tensorboardX import SummaryWriter

from backbone.model_irse import IR_50
from backbone.model_resnet import ResNet_50
from config import configurations
from head.metrics import ArcFace
from loss.focal import FocalLoss
from util.utils import (AverageMeter, accuracy, get_time,
                        make_weights_for_balanced_classes,
                        schedule_lr, separate_irse_bn_paras,
                        separate_resnet_bn_paras, warm_up_lr)
from MaskTheFace.mask_transform import Mask_transform, changeWorkingDirectory
import warnings
warnings.filterwarnings("once")


from numpy.core import sqrt as sqrt
def normalize_feat(x:np.ndarray) -> np.ndarray:
    x = x.ravel(order='K').astype('float32')
    return x / sqrt(x.dot(x))




#!======= hyperparameters & data loaders =======#
cfg = configurations[2]

SEED                 = cfg['SEED'                ] # random seed   for reproduce results
DATA_ROOT            = cfg['DATA_ROOT'           ] # the    parent     root      where   your train/val/test data are stored
MODEL_ROOT           = cfg['MODEL_ROOT'          ] # the    root       to        buffer  your checkpoints
LOG_ROOT             = cfg['LOG_ROOT'            ] # the    root       to        log     your train/val status
BACKBONE_RESUME_ROOT = cfg['BACKBONE_RESUME_ROOT'] # the    root       to        resume  training from a saved checkpoint
HEAD_RESUME_ROOT     = cfg['HEAD_RESUME_ROOT'    ] # the    root       to        resume  training from a saved checkpoint

BACKBONE_NAME  = cfg['BACKBONE_NAME'  ] # support: ['ResNet_50', 'ResNet_101', 'ResNet_152', 'IR_50', 'IR_101', 'IR_152', 'IR_SE_50', 'IR_SE_101', 'IR_SE_152']
HEAD_NAME      = cfg['HEAD_NAME'      ] # support: ['Softmax' , 'ArcFace' , 'CosFace', 'SphereFace', 'Am_softmax' ]
LOSS_NAME      = cfg['LOSS_NAME'      ] # support: ['Focal' , 'Softmax' ]

INPUT_SIZE     = cfg['INPUT_SIZE'     ]
RGB_MEAN       = cfg['RGB_MEAN'       ] # for normalize inputs
RGB_STD        = cfg['RGB_STD'        ]
EMBEDDING_SIZE = cfg['EMBEDDING_SIZE' ] # feature dimension

BATCH_SIZE     = cfg['BATCH_SIZE'     ]
DROP_LAST      = cfg['DROP_LAST'      ] # whether drop the last batch to ensure consistent batch_norm statistics
LR             = cfg['LR'             ] # initial LR
NUM_EPOCH      = cfg['NUM_EPOCH'      ]
EPOCH_START    = cfg['EPOCH_START'    ]
WEIGHT_DECAY   = cfg['WEIGHT_DECAY'   ]
MOMENTUM       = cfg['MOMENTUM'       ]
STAGES         = cfg['STAGES'         ] # epoch stages to decay learning rate
DEVICE         = cfg['DEVICE'         ]
MULTI_GPU      = cfg['MULTI_GPU'      ] # flag to use multiple GPUs
GPU_ID         = cfg['GPU_ID'         ] # specify your GPU ids
PIN_MEMORY     = cfg['PIN_MEMORY'     ]
NUM_WORKERS    = cfg['NUM_WORKERS'    ]


backbone = IR_50(INPUT_SIZE)
backbone.load_state_dict(torch.load('model/Backbone_IR_50_Epoch_7.pth', map_location='cpu'))
val_transform = T.Compose([
    T.Resize(INPUT_SIZE),
    T.ToTensor(),
    T.Normalize(mean = RGB_MEAN, std = RGB_STD),
])

val_dataset = datasets.ImageFolder('data/14_11', val_transform)
val_loader = DataLoader(val_dataset, 8, shuffle=True, num_workers=os.cpu_count())

@torch.inference_mode()
def validate(_backbone:nn.Module, val_loader:DataLoader, 
             device:torch.device=torch.device('cpu'),
             feats_path:str = ..., class_path:str = ...):
    """
    run inference tạo feat và label , save vào 2 file feat.npy và cls.npy
    """
    backbone = deepcopy(_backbone).to(device=device)
    backbone.eval()
    
    feats = np.empty((0, 512))
    classes = np.empty(0, dtype=np.uint32)
    for datas, targets in val_loader:
        _feats = backbone(datas.to(device)).detach().cpu().numpy()
        _cls = targets.numpy().astype(np.uint32)
        feats = np.concatenate((feats, _feats), axis=0)
        classes = np.concatenate((classes, _cls), axis=0)
    np.save(feats_path, feats)
    np.save(class_path, classes)
    
# validate(backbone, val_loader, torch.device('cpu'))
